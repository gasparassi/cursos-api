<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserTypeController;
use App\Http\Controllers\AddressController;
use App\Http\Controllers\CurseController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\InscriptionController;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */
Route::get('/', function () {
    return response()
                    ->json([
                        'message' => 'Cursos API',
                        'status' => 'Connected',
                        'statusCode' => 200,
                            ],
                            200);
}
);

Route::group([
    'prefix' => 'v1',
        ], function () {    
    Route::group([
        'prefix' => 'inscriptions',
            ], function () {
        Route::get('/search', [InscriptionController::class, 'indexAllByParameter']);
        Route::patch('/status', [InscriptionController::class, 'udpateStatus']);
    });    
    Route::group([
        'prefix' => 'addresses',
            ], function () {
        Route::get('/{cep}', [AddressController::class, 'requestFullAddressFromExternApi']);
    });
    Route::resource('inscriptions', InscriptionController::class)->except(['create', 'edit']);
    Route::resource('user_types', UserTypeController::class)->except(['create', 'edit']);
    Route::resource('users', UserController::class)->except(['create', 'edit']);    
    Route::resource('curses', CurseController::class)->except(['create', 'edit']);
});

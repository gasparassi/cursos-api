<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInscriptionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inscriptions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->id();
            $table->foreignId('curse_id')->unsigned();
            $table->foreignId('user_id')->unsigned();
            $table->foreignId('user_type_id')->unsigned();
            $table->string('company', 45);
            $table->string('status', 45);
            $table->timestamps();
            $table->foreign('curse_id')->references('id')->on('curses');
            $table->foreign('user_type_id')->references('id')->on('curses');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inscriptions');
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\InscriptionService;
use App\Http\Requests\InscriptionStroreRequest;
use App\Http\Resources\InscriptionResource;
use App\Http\Requests\InscriptionUpdateAddressRequest;

class InscriptionController extends Controller
{

    protected $service;

    function __construct(InscriptionService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $inscriptions = $this->service->getAllResources();
            if ( $inscriptions !== null ) {
                return response()->json([
                            'data' => InscriptionResource::collection($inscriptions),
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'message' => 'Nenhuma inscrição realizada.',
                            'statusCode' => 404
                                ], 404);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    public function indexAllByParameter(Request $param)
    {
        try {
            $inscriptions = $this->service->getAllResourcesByParameter($param);
            if ( $inscriptions !== null ) {
                return response()->json([
                            'data' => InscriptionResource::collection($inscriptions),
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'message' => 'Nenhuma inscrição encontrada com os parâmetros informados.',
                            'statusCode' => 404
                                ], 404);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\InscriptionStroreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InscriptionStroreRequest $request)
    {
        try {
            $inscription = $this->service->createNewResource($request->all());
            if ( $inscription !== null ) {
                return response()->json([
                            'data' => new InscriptionResource($inscription),
                            'statusCode' => 201,
                                ], 201);
            } else {
                return response()->json([
                            'message' => 'Erro ao realizar a inscrição.',
                            'statusCode' => 422
                                ], 422);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $inscription = $this->service->getOneResourceById($id);
            if ( $inscription !== null ) {
                return response()->json([
                            'data' => new InscriptionResource($inscription),
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'message' => 'Inscrição não encontrada.',
                            'statusCode' => 404
                                ], 404);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\InscriptionUpdateAddressRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(InscriptionUpdateAddressRequest $request, $id)
    {
        try {
            $inscription = $this->service->updateResource($request->all(), $id);
            if ( $inscription !== null ) {
                return response()->json([
                            'data' => new InscriptionResource($inscription),
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'message' => 'Inscrição não encontrada.',
                            'statusCode' => 404
                                ], 404);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }
    
    public function udpateStatus(Request $request, $id)
    {
        try {
            $inscription = $this->service->updataStatusInscription($request->all(), $id);
            if ( $inscription !== null ) {
                return response()->json([
                            'data' => new InscriptionResource($inscription),
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'message' => 'Inscrição não encontrada.',
                            'statusCode' => 404
                                ], 404);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = $this->service->destroyResource($id);
            if ( $data ) {
                return response()->json([
                            'data' => 'Inscrição removida com sucesso.',
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'message' => 'Inscrição não encontrada.',
                            'statusCode' => 404
                                ], 404);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

}

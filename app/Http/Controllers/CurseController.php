<?php

namespace App\Http\Controllers;

use App\Services\CurseService;
use App\Http\Requests\CurseRequest;
use App\Http\Resources\CurseResource;

class CurseController extends Controller
{

    protected $service;

    function __construct(CurseService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $curses = $this->service->getAllResources();
            if ( $curses !== null ) {
                return response()->json([
                            'data' => CurseResource::collection($curses),
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'message' => 'Nenhum curso cadastrado.',
                            'statusCode' => 404
                                ], 404);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CurseRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CurseRequest $request)
    {
        try {
            $curse = $this->service->createNewResource($request->all());
            if ( $curse !== null ) {
                return response()->json([
                            'data' => new CurseResource($curse),
                            'statusCode' => 201,
                                ], 201);
            } else {
                return response()->json([
                            'message' => 'Erro ao cadastrar o curso',
                            'statusCode' => 422
                                ], 422);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $curse = $this->service->getOneResourceById($id);
            if ( $curse !== null ) {
                return response()->json([
                            'data' => new CurseResource($curse),
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'message' => 'Curso não encontrado.',
                            'statusCode' => 404
                                ], 404);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\CurseRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CurseRequest $request, $id)
    {
        try {
            $curse = $this->service->updateResource($request->all(), $id);
            if ( $curse !== null ) {
                return response()->json([
                            'message' => new CurseResource($curse),
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'message' => 'Curso não encontrado.',
                            'statusCode' => 404
                                ], 404);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = $this->service->destroyResource($id);
            if ( $data ) {
                return response()->json([
                            'data' => 'Curso removido com sucesso.',
                            'statusCode' => 200,
                                ], 200);
            } else {
                return response()->json([
                            'message' => 'Curso não encontrado.',
                            'statusCode' => 404
                                ], 404);
            }
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro não previsto.',
                        'error' => $ex->getMessage(),
                        'statusCode' => 500
                            ], 500);
        }
    }

}

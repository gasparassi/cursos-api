<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'logradouro' => $this['logradouro'],
            'bairro' => $this['bairro'],
            'localidade' => $this['localidade'],
            'uf' => $this['uf'],
            'cep' => $this['cep'],
            'ddd' => $this['ddd'],
            'ibge' => $this['ibge'],
            'numero' => $this['numero'],
            'complemento' => $this['complemento'],
        ];
    }

}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CurseResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'value' => $this->value,
            'date_start_registrations' => $this->date_start_registrations,
            'date_end_registrations' => $this->date_end_registrations,
            'max_number_subscribers' => $this->max_number_subscribers,
        ];
    }

}

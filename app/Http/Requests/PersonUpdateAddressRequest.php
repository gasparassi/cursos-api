<?php

namespace App\Http\Requests;

use App\Http\Requests\FormRequestCustom as FormRequest;

class PersonUpdateAddressRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'person_id' => 'required|integer',
            'logradouro' => 'required|string|max:100',
            'numero' => 'required|integer',
            'bairro' => 'required|string|max:45',
            'uf' => 'required|string|max:2',
            'complemento' => 'required|string|max:45',
            'cep' => 'required|string|max:9',
            'ddd' => 'required|integer',
            'ibge' => 'required|integer',
        ];
    }

}

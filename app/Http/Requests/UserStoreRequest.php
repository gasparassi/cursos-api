<?php

namespace App\Http\Requests;

use App\Http\Requests\FormRequestCustom as FormRequest;

class UserStoreRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_type_id' => 'required|integer',            
            'email' => 'required|string|unique:users|max:100|regex:/^.+@.+$/i|email:rfc,dns,filter',
            'password' => 'required|min:6|confirmed',
        ];
    }

}

<?php

namespace App\Http\Requests;

use App\Http\Requests\FormRequestCustom as FormRequest;
use Illuminate\Validation\Rule;
use App\Models\User;

class UserUpdateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Rule::unique((new User)->getTable())->ignore($this->route()->user->id ?? null),
            'user_type_id' => 'required|integer',
            'name' => 'required|string|max:100',
            'email' => 'required|string|max:100|regex:/^.+@.+$/i|email:rfc,dns,filter',            
            'password' => 'required|min:6|confirmed',
        ];
    }

}

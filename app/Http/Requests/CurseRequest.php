<?php

namespace App\Http\Requests;

use App\Http\Requests\FormRequestCustom as FormRequest;

class CurseRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:45',
            'description' => 'required|string|max:45',
            'value' => 'required',
            'date_start_registrations' => 'required|date',
            'date_end_registrations' => 'required|date',
            'max_number_subscribers' => 'required|integer',
            'material' => 'string|max:100',
        ];
    }

}

<?php

namespace App\Http\Requests;

use App\Http\Requests\FormRequestCustom as FormRequest;

class InscriptionStroreRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [            
            'curse_id' => 'required|integer',
            'user_type_id' => 'required|integer',
            'name' => 'required|string|max:100',
            'email' => 'required|string|max:100|regex:/^.+@.+$/i|email:rfc,dns,filter',
            'password' => 'required|min:6|confirmed',
            'logradouro' => 'required|string|max:100',
            'numero' => 'required|integer',
            'bairro' => 'required|string|max:45',
            'uf' => 'required|string|max:2',
            'complemento' => 'required|string|max:45',
            'cep' => 'required|string|max:9',
            'ddd' => 'required|integer',
            'ibge' => 'required|integer',
            'cpf' => 'required|string|size:11',
            'company' => 'required|string|max:45',
            'phone' => 'required|string|size:10',
            'celphone' => 'required|string|size:11',
        ];
    }

}

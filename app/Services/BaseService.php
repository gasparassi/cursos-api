<?php

namespace App\Services;

/**
 *
 * @author eder
 */
interface BaseService
{

    /**
     * Cria um novo recurso no banco de dados
     * 
     * @param array $resource
     */
    public function createNewResource(array $resource);

    /**
     * Recupera todos os recursos do banco de dados
     */
    public function getAllResources();

    /**
     * Recupera um recurso do banco de dados utilizando por parâmetro de 
     * pesquisa o id único do registro
     * 
     * @param int $id
     */
    public function getOneResourceById(int $id);

    /**
     * Atualiza um recurso no banco de dados
     * 
     * @param array $resource
     * @param int $id
     */
    public function updateResource(array $resource, int $id);

    /**
     * Remove um recurso no banco de dados
     * 
     * @param int $id
     */
    public function destroyResource(int $id);
}

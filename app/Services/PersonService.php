<?php

namespace App\Services;

use App\Services\BaseService;
use App\Models\Person;

/**
 * Description of PersonService
 *
 * @author eder
 */
class PersonService implements BaseService
{

    protected $entity;

    function __construct(Person $model)
    {
        $this->entity = $model;
    }

    /**
     * Obtém o endereço exato que cadastrado na API externa
     * 
     * @param array $resource
     * @return array || null
     */
    private function getAddressValid(array $resource)
    {
        $addressService = new AddressService($this->entity->address()->getModel());

        return $addressService->requestFullAddressFromExternApi($resource);
    }
    
    private function mountedPersonArray($resource, $addressId)
    {
        $personArray['address_id'] = $addressId;
        $personArray['name'] = $resource['name'];
        $personArray['cpf'] = $resource['cpf'];
        $personArray['phone'] = $resource['phone'];
        $personArray['celphone'] = $resource['celphone'];
        
        return $personArray;
    }

    public function createNewResource(array $resource)
    {
        $addressValidFromApi = $this->getAddressValid($resource);

        if ( $addressValidFromApi !== null ) {
            $address = $this->entity->address()->getModel()->create($addressValidFromApi);
            if ( $address !== null ) {                
                return $this->entity->create($this->mountedPersonArray($resource, $address->id));
            }
        }
    }

    public function destroyResource(int $id)
    {
        $person = $this->getOneResourceById($id);
        if ( $person !== null ) {
            $person->delete();
            return true;
        } else {
            return false;
        }
    }

    public function getAllResources()
    {
        throw new Exception('Method not implemented');
    }

    public function getOneResourceById(int $id)
    {
        return $this->entity->find($id);
    }

    public function updateResource(array $resource, int $id)
    {
        $person = $this->getOneResourceById($id);

        $addressValidFromApi = $this->getAddressValid($resource);

        if ( $addressValidFromApi !== null ) {
            $addressService = new AddressService($this->entity->address()->getModel());
            $addressUpdate = $addressService->updateResource($addressValidFromApi, $person->address_id);
            if ( $addressUpdate !== null ) {
                $this->entity->update($this->mountedPersonArray($resource));
                
                return $this->getOneResourceById($id);
            }
        }
        return $person;
    }

}

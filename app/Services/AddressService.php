<?php

namespace App\Services;

use App\Models\Address;
use App\Services\ViacepService;
use App\Services\PostmonService;

/**
 * Description of AddressService
 *
 * @author eder
 */
class AddressService
{

    protected $entity;
    protected $numberAddress;
    protected $ComplementAddress;

    function __construct(Address $model)
    {
        $this->entity = $model;
        $this->numberAddress = 0;
        $this->ComplementAddress = '';
    }

    public function getOneResourceById(int $id)
    {
        return $this->entity->find($id);
    }

    public function updateResource(array $resource, int $id)
    {
        $address = $this->getOneResourceById($id);
        if ( $address !== null ) {
            $addressArray = $this->mountedArrayFroCreateNewAddress($resource);
            $address->update($addressArray);
            return $this->getOneResourceById($id);
        } else {
            return null;
        }
    }

    private function mountedArrayFroCreateNewAddress(array $resource)
    {
        $addressArray['logradouro'] = $resource['logradouro'];
        $addressArray['numero'] = $resource['numero'];
        $addressArray['bairro'] = $resource['bairro'];
        $addressArray['localidade'] = $resource['localidade'];
        $addressArray['uf'] = $resource['uf'];
        $addressArray['complemento'] = $resource['complemento'];
        $addressArray['cep'] = $resource['cep'];
        $addressArray['ddd'] = $resource['ddd'];
        $addressArray['ibge'] = $resource['ibge'];

        return $addressArray;
    }

    /**
     * Recupera o endereço válido do cliente para posterior cadastro
     * 
     * @param array $resource
     * @return array || null
     */
    public function requestFullAddressFromExternApi(array $resource)
    {
        if ( count($resource) > 1 ) {
            $addressArray = $this->mountedArrayFroCreateNewAddress($resource);
        } else {
            $addressArray = $resource;
        }

        $cep = $addressArray['cep'];

        $viaCepService = new ViacepService();
        $addressFromApi = $viaCepService->getFullAddress($cep, 'json');
        if ( ( $addressFromApi !== null ) && (!isset($addressFromApi['erro']) ) ) {
            return $this->compareAddressClientFromApi($addressFromApi, $addressArray);
        } else {
            $postManService = new PostmonService();
            $addressFromApi = $postManService->getFullAddress($cep, 'json');
            if ( $addressFromApi !== null ) {
                return $this->compareAddressClientFromApi($addressFromApi, $addressArray);
            } else {
                return null;
            }
        }
    }

    /**
     * Faz a comparação do endereço informado pelo cliente entre o recuperado
     * pela API externa
     * 
     * @param array $addressFromApi
     * @param array $addressFromClient
     * @return array
     */
    private function compareAddressClientFromApi(array $addressFromApi, array $addressFromClient)
    {
        $addressFromClientMounted = $this->getArrayAddressFromClient($addressFromClient);

        $compareAddress = array_diff($addressFromApi, $addressFromClientMounted);

        if ( count($compareAddress) > 0 ) {
            foreach ($compareAddress as $key => $value) {
                $addressFromClient[$key] = $value;
            }
        }

        return $this->mountedArrayFullAddress($addressFromClient);
    }

    private function mountedArrayFullAddress(array $addressFromClient)
    {
        $addressFromClient['numero'] = $this->numberAddress;
        $addressFromClient['complemento'] = $this->ComplementAddress;

        return $addressFromClient;
    }

    private function getArrayAddressFromClient(array $addressFromClient)
    {
        if ( isset($addressFromClient['numero']) ) {
            $this->numberAddress = $addressFromClient['numero'];
            unset($addressFromClient['numero']);
        }

        if ( isset($addressFromClient['complemento']) ) {
            $this->ComplementAddress = $addressFromClient['complemento'];
            unset($addressFromClient['complemento']);
        }

        return $addressFromClient;
    }

}

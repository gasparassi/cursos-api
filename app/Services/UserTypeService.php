<?php

namespace App\Services;

use App\Services\BaseService;
use App\Models\UserType;

/**
 * Description of UserTypeService
 *
 * @author eder
 */
class UserTypeService implements BaseService
{

    protected $entity;

    function __construct(UserType $model)
    {
        $this->entity = $model;
    }

    public function createNewResource(array $resource)
    {
        return $this->entity->create($resource);
    }

    public function destroyResource(int $id)
    {
        $userType = $this->getOneResourceById($id);
        if ( $userType !== null ) {
            $userType->delete();
            return true;
        } else {
            return false;
        }
    }

    public function getAllResources()
    {
        $userTypes = $this->entity->with('users')->get();
        return count($userTypes) > 0 ? $userTypes : null;
    }

    public function getOneResourceById(int $id)
    {
        return $this->entity->find($id);
    }

    public function updateResource(array $resource, int $id)
    {
        $userType = $this->getOneResourceById($id);

        if ( $userType !== null ) {
            $userType->update($resource);
            return $this->getOneResourceById($id);
        }
        
        return $userType;
    }

}

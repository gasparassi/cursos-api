<?php

namespace App\Services;

use App\Services\BaseService;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

/**
 * Description of UserService
 *
 * @author eder
 */
class UserService implements BaseService
{

    protected $entity;

    function __construct(User $model)
    {
        $this->entity = $model;
    }

    private function createNewPersonByUser(array $resource)
    {
        $personService = new PersonService($this->entity->person()->getModel());

        return $personService->createNewResource($resource);
    }
    
    private function getPersonIdByCpfIfExists($resource)
    {
        $personByCpf = $this->entity->person()->getModel()
                ->select(['id', 'cpf'])
                ->where('cpf', $resource['cpf'])->first();
        
        return $personByCpf;
    }
    
    private function getPersonId($resource)
    {
        $personByCpf = $this->getPersonIdByCpfIfExists($resource);
        
        $personId = 0;
        
        if ( $personByCpf !== null ) {
            $personId = $personByCpf->id;
        } else {
            $person = $this->createNewPersonByUser($resource);
            if ( $person !== null ) {
                $personId = $person->id;
            } else {
                return null;
            }
        }
        
        return $personId;
    }
    
    private function mountedUserArray($resource)
    {
        $userArray['person_id'] = $this->getPersonId($resource);
        $userArray['user_type_id'] = $resource['user_type_id'];
        $userArray['email'] = $resource['email'];
        $userArray['password'] = Hash::make($resource['password']);

        return $userArray;
    }
    
    public function createNewResource(array $resource)
    {        
        return $this->entity->create($this->mountedUserArray($resource));
    }

    public function destroyResource(int $id)
    {
        $user = $this->getOneResourceById($id);
        if ( $user !== null ) {
            $user->delete();
            return true;
        } else {
            return false;
        }
    }

    public function getAllResources()
    {
        $users = $this->entity->with(['userType'])->get();
        return count($users) > 0 ? $users : null;
    }

    public function getOneResourceById(int $id)
    {
        return $this->entity->find($id);
    }

    public function updateResource(array $resource, int $id)
    {
        $user = $this->getOneResourceById($id);

        if ( $user !== null ) {
            $resource['password'] = Hash::make($resource['password']);
            $user->update($resource);

            return $this->getOneResourceById($id);
        }

        return $user;
    }

}

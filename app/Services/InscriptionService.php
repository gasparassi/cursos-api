<?php

namespace App\Services;

use App\Services\BaseService;
use App\Models\Inscription;

/**
 * Description of InscriptionService
 *
 * @author eder
 */
class InscriptionService implements BaseService
{

    protected $entity;

    function __construct(Inscription $model)
    {
        $this->entity = $model;
    }

    private function createNewUserByInscription(array $resource)
    {
        $userService = new UserService($this->entity->user()->getModel());

        return $userService->createNewResource($resource);
    }
    
    private function getUserIdByEmailIfExists($resource)
    {
        $userByEmail = $this->entity->user()->getModel()->select(['id', 'email'])
                ->where('email', $resource['email'])->first();
        
        return $userByEmail;
    }
    
    private function getUserId($resource)
    {
        $userByEmail = $this->getUserIdByEmailIfExists($resource);
        
        $userId = 0;
        
        if ( $userByEmail !== null ) {
            $userId = $userByEmail->id;
        } else {
            $user = $this->createNewUserByInscription($resource);
            if ( $user !== null ) {
                $userId = $user->id;
            } else {
                return null;
            }
        }
        
        return $userId;
    }
    
    
    private function mountedInscriptionArray($resource)
    {
        $inscriptionArray['user_id'] = $this->getUserId($resource);
        $inscriptionArray['curse_id'] = $resource['curse_id'];
        $inscriptionArray['user_type_id'] = $resource['user_type_id'];
        $inscriptionArray['company'] = $resource['company'];
        $inscriptionArray['status'] = 'Aguardando pagamento';
        
        return $inscriptionArray;
    }

    public function createNewResource(array $resource)
    {
        return $this->entity->create($this->mountedInscriptionArray($resource));
    }

    public function destroyResource(int $id)
    {
        $inscription = $this->getOneResourceById($id);
        if ( $inscription !== null ) {
            $inscription->delete();

            return true;
        } else {
            return false;
        }
    }

    public function getAllResourcesByParameter($request)
    {
        $inscriptions = $this->entity->where($request->all())->get();
        return count($inscriptions) > 0 ? $inscriptions : null;
    }

    public function getAllResources()
    {
        $inscriptions = $this->entity->with(['user', 'curse'])->get();
        return count($inscriptions) > 0 ? $inscriptions : null;
    }

    public function getOneResourceById(int $id)
    {
        return $this->entity->find($id);
    }

    public function updateResource(array $resource, int $id)
    {
        $inscription = $this->getOneResourceById($id);

        if ( $inscription !== null ) {
            $inscription->update($resource);

            return $this->getOneResourceById($id);
        }

        return $inscription;
    }
    
    public function updataStatusInscription($request, $id)
    {
        $inscription = $this->getOneResourceById($id);
        if ( $inscription !== null ) {
            $inscription->status = $request->status;
            $inscription->save();
            
            return true;
        }
        
        return false;
    }

}

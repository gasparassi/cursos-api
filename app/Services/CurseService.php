<?php

namespace App\Services;

use App\Services\BaseService;
use App\Models\Curse;

/**
 * Description of CurseService
 *
 * @author eder
 */
class CurseService implements BaseService
{

    protected $entity;

    function __construct(Curse $model)
    {
        $this->entity = $model;
    }

    public function createNewResource(array $resource)
    {
        return $this->entity->create($resource);
    }

    public function destroyResource(int $id)
    {
        $curse = $this->getOneResourceById($id);
        if ( $curse !== null ) {
            $curse->delete();
            return true;
        } else {
            return false;
        }
    }

    public function getAllResources()
    {
        $curses = $this->entity->all();
        return count($curses) > 0 ? $curses : null;
    }

    public function getAllResourcesWithRelationships()
    {
        $curses = $this->entity->with('inscriptions')->get();
        return count($curses) > 0 ? $curses : null;
    }

    public function getOneResourceById(int $id)
    {
        return $this->entity->find($id);
    }

    public function getOneResourceByIdWithRelationships(int $id)
    {
        return $this->entity->with('inscriptions')->find($id);
    }

    public function updateResource(array $resource, int $id)
    {
        $curse = $this->getOneResourceById($id);

        if ( $curse !== null ) {
            $curse->update($resource);
            return $this->getOneResourceById($id);
        }
        
        return $curse;
    }

}

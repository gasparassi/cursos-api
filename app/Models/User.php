<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{

    use HasFactory;

    protected $table = "users";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_type_id',
        'person_id',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ["password",];

    public function userType()
    {
        return $this->belongsTo(UserType::class, 'user_type_id', 'id');
    }
    
    public function person()
    {
        return $this->hasOne(Person::class, 'id', 'person_id');
    }

}

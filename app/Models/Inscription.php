<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inscription extends Model
{
    use HasFactory;

    protected $table = "inscriptions";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'curse_id',
        'user_id',
        'user_type_id',
        'status',
        'company',
        'status',
    ];

    public function curse()
    {
        return $this->belongsTo(Curse::class);
    }
    
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Curse extends Model
{

    use HasFactory;

    protected $table = "curses";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'value',
        'date_start_registrations',
        'date_end_registrations',
        'max_number_subscribers',
        'material',
    ];

    public function inscriptions()
    {
        return $this->hasMany(Inscription::class);
    }

}
